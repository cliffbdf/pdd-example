# pdd-example

The pdd-example product is composed of these components:
* [pdd-example-serverless](https://gitlab.com/cliffbdf/pdd-example-serverless) - an AWS Lambda client.
* [pdd-example-compa](https://gitlab.com/cliffbdf/pdd-example-compa) - a gRPC microservice.
* [pdd-example-compb](https://gitlab.com/cliffbdf/pdd-example-compb) - a jar library that is embedded in both the serverless client and in compa.

Each component has its own repo, and may be used by other products. Each repo contains component level tests for that component.

This repo contains product level tests, including,
* end-to-end integration acceptance tests.
* failure mode tests, that verify the resiliency of the product as a whole.

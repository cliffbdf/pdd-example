# language: en

Feature: ID file parsing

	@story1.1
	Scenario: Parse one fileNamePrim
		Given a file containing one user ID per line
		When I parse the file
		Then the file is read through to the end
		And if a user ID is invalid syntax
		Then an error is logged for that file line

	@story1.2
	Scenario: Parse two files
		Given two files, each containing one user ID per line
		When I parse the two files
		Each is parsed, with the same acceptance criteria as specified in the previous scenario.

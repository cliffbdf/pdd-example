# Makefile for running end-to-end "system level" tests.
# Assumes that env variables been set, to set tool paths and credentials.
# (For local development, source env.mac)

export CompAVersion := 1.0
export CompBVersion := 1.0

all: clean jar cluster namespace testbench acctest failtest getresults

.PHONY: all

.ONESHELL

# Build the tests.
jar:
	$(MVN) install

# Verify we have a kubernetes cluster: if not, create one.
cluster:

# Create a kubernetes namespace for these tests.
namespace:

# Deploy all components needed for system-wide testing to the namespace.
testbench:

# Perform system-level acceptance tests.
acctest:

# Perform system-level failure mode tests.
failtest:

# Retrieve test results.
getresults:

# Remove all test artifacts.
clean:
